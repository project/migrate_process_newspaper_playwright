<?php

namespace Drupal\migrate_process_newspaper_playwright\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Twodareis2do\ScrapeNewspaperPlaywright\NewspaperPlaywrightWrapper;

/**
 * Provides a 'MigrateProcessNewspaperPlaywright' migrate process plugin.
 *
 * Example:
 *
 * @code
 *
 * process:
 *   'body/value':
 *     -
 *       plugin: get
 *       source: link
 *     -
 *       plugin: migrate_process_newspaper_playwright
 *       debug: true // default false
 *       cwd: "/my/python/script" // default null
 *     -
 *       plugin: skip_on_empty
 *       method: row
 *       message: 'migrate_process_newspaper_playwright import failed'
 *     -
 *       plugin: extract
 *       index:
 *         - text
 *
 * @endcode
 *
 * @see \Drupal\migrate\Plugin\MigrateProcessInterface
 *
 * @MigrateProcessPlugin(
 *  id = "migrate_process_newspaper_playwright"
 * )
 */
class MigrateProcessNewspaperPlaywright extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The Newspaper Playwright wrapper client.
   *
   * @var \Twodareis2do\ScrapeNewspaperPlaywright\NewspaperPlaywrightWrapper
   */
  protected $parser;

  /**
   * The Logger Class object.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a database object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger;

  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    $http_string_position = strpos($value ?? '', 'http');

    // Disable Debugging by default.
    $debug = FALSE;
    $configuration = $this->configuration;

    // Check if we need to enable debugging.
    if (isset($configuration["debug"]) && $configuration["debug"] === TRUE) {
      $debug = TRUE;
    }
    // This is useful as we can also set from the unit tests.
    if (isset($this->pluginDefinition) && isset($this->pluginDefinition["debug"]) && filter_var($this->pluginDefinition["debug"], FILTER_VALIDATE_BOOLEAN) === TRUE) {
      $debug = TRUE;
    }
    // Check if we need to pass a the current working directory (relative).
    if (isset($configuration["cwd"])) {
      $cwd = $configuration["cwd"];
    }
    // Check if url is absolute (not relative) and is a url.
    if ($http_string_position === 0 && filter_var($value, FILTER_VALIDATE_URL) !== FALSE) {
      try {

        if (!isset($this->parser)) {
          $this->parser = new NewspaperPlaywrightWrapper();
        }
        // If debugging will output json to /tmp directory.
        if (isset($cwd)) {
          $output = $this->parser->scrape($value, $debug, $cwd);
        }
        else {
          $output = $this->parser->scrape($value, $debug);
        }

        return $output;

      }
      catch (\Exception $e) {

        // Logs a notice to channel if we get http error response.
        $this->logger->notice('Newspaper Playwright Failed to get (1) URL @url "@error". @code', [
          '@url' => $value,
          '@error' => $e->getMessage(),
          '@code' => $e->getCode(),
        ]);

        return '';
      }
    }
    else {
      // If string does not start with http return empty string.
      return '';
    }

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.channel.migrate_process_newspaper_playwright')
    );
  }

}
