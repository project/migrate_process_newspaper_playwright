<?php

declare(strict_types = 1);

namespace Drupal\Tests\migrate_process_newspaper_playwright\Unit\process;

use Drupal\migrate_process_newspaper_playwright\Plugin\migrate\process\MigrateProcessNewspaperPlaywright;
use Drupal\Tests\migrate\Unit\process\MigrateProcessTestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Psr\Log\LoggerInterface;

/**
 * Tests the migrate_process_newspaper_playwright process plugin.
 *
 * @group migrate_process_newspaper_playwright
 * @coversDefaultClass \Drupal\migrate_process_newspaper3k\Plugin\migrate\process\MigrateProcessNewspaper3kTest
 */
final class MigrateProcessNewspaperPlaywrightTest extends MigrateProcessTestCase {

  /**
   * A logger prophecy object.
   *
   * Using ::setTestLogger(), this prophecy will be configured and injected into
   * the container. Using $this->logger->function(args)->shouldHaveBeenCalled()
   * you can assert that the logger was called.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected $logger;

  /**
   * Mock of http_client service.
   *
   * @var \GuzzleHttp\Client
   */
  protected $mockHttp;

  /**
   * The data fetcher plugin definition.
   */
  private array $pluginDefinition = [
    'id' => 'migrate_process_newspaper_playwright',
    'title' => 'Migrate Process Newspaper Playwright',
    'debug' => TRUE,
    'cwd' => NULL,
  ];

  /**
   * Test MigrateProcessnewspaper3k returns json.
   */
  public function testMigrateProcessNewspaperPlaywright() {
    $configuration = [];
    $value = 'https://www.skysports.com/football/news/11661/13154394/jarrad-branthwaite-transfer-news-everton-value-centre-back-at-more-than-70m-amid-man-utd-interest';

    // $html_with_cookie = file_get_contents(__DIR__ . '/../../../fixtures/files/sky_jarrad_branthwaite_transfer_news_everton.html');
    $html = file_get_contents(__DIR__ . '/../../../fixtures/files/sky_jarrad_branthwaite_transfer_news_everton_full.html');
    $json = file_get_contents(__DIR__ . '/../../../fixtures/files/debug_www.skysports.com_1718208309010.json');
    $associative_array = json_decode($json, TRUE);
    $mock = new MockHandler([
      new Response(200, [], $html),
    ]);
    $handler = HandlerStack::create($mock);
    $this->mockHttp = new Client(['handler' => $handler]);

    $this->logger = $this->prophesize(LoggerInterface::class);

    $document = (new MigrateProcessNewspaperPlaywright($configuration, 'migrate_process_newspaper_playwright', $this->pluginDefinition, $this->mockHttp, $this->logger->reveal()))
      ->transform($value, $this->migrateExecutable, $this->row, 'destinationproperty');

    $this->assertEquals($associative_array, $document, $message = "actual value is not equal to expected");
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    $this->row = $this->getMockBuilder('Drupal\migrate\Row')
      ->disableOriginalConstructor()
      ->getMock();

    $this->migrateExecutable = $this->getMockBuilder('Drupal\migrate\MigrateExecutable')
      ->disableOriginalConstructor()
      ->getMock();

    $mockHttp = $this
      ->getMockBuilder('GuzzleHttp\Client')
      ->disableOriginalConstructor()
      ->addMethods(['getBody'])
      ->onlyMethods(['get'])
      ->getMock();
    $this->mockHttp = $mockHttp;

    parent::setUp();

  }

}
